//
//  OWForecastDecodableTest.swift
//  OpenWeatherMapForecastTests
//
//  Created by Mantas Jakimavicius on 09/04/19.
//

import XCTest
@testable import OpenWeatherMapForecast

class OWForecastDecodableTest: XCTestCase {

    var sut: OWForecast!
    
    private func getForecaseJSON() -> Data? {
        guard let path = Bundle(for: type(of: self)).path(forResource: "forecast", ofType: "json") else {
            return nil
        }
        let url = URL(fileURLWithPath: path)
        do {
            return try Data(contentsOf: url, options: .mappedIfSafe)
        } catch {
            return nil
        }
    }

    override func tearDown() {
        sut = nil
    }
    
    func test_Decoding_Success() {
        guard let data = getForecaseJSON() else {
            assertionFailure()
            return
        }
        do {
            let forecast = try JSONDecoder().decode(OWForecastAPI_Response.self, from: data)
            let firstForecast = forecast.list?.first
            XCTAssertNotNil(firstForecast)
            XCTAssertEqual(firstForecast!.date, 1554811200)
            XCTAssertEqual(firstForecast!.temperature, 20.01)
            XCTAssertEqual(firstForecast!.temperature_minimum, 16.49)
            XCTAssertEqual(firstForecast!.temperature_maximum, 20.01)
            XCTAssertEqual(firstForecast!.pressure, 1006.19)
            XCTAssertEqual(firstForecast!.sea_level, 1006.19)
            XCTAssertEqual(firstForecast!.ground_level, 990.52)
            XCTAssertEqual(firstForecast!.humidity, 78)
        } catch {
            assertionFailure()
            print(error)
        }
    }
}
