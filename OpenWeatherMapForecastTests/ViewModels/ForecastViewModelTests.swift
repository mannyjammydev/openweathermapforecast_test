//
//  ForecastViewModelTests.swift
//  OpenWeatherMapForecastTests
//
//  Created by Mantas Jakimavicius on 09/04/19.
//

import XCTest
@testable import OpenWeatherMapForecast

class ForecastViewModelTests: XCTestCase {
    
    var sut: ForecastViewModel!
    
    override func setUp() {
        
    }
    
    override func tearDown() {
        sut = nil
    }
    
    private func getMockForecast() -> OWForecast {
        var forecast = OWForecast()
        forecast.date = 978345000
        
        var main = OWForecast.Main()
        main.temperature = 26.2
        main.temperature_minimum = 10.2
        main.temperature_maximum = 29.4
        main.pressure = 1002.18
        main.sea_level = 1003.19
        main.ground_level = 998.77
        main.humidity = 78
        
        forecast.set(main)
        return forecast
    }

    func test_Model_Initialisation_Success() {
        sut = ForecastViewModel(with: getMockForecast(), in: TimeZone(abbreviation: "UTC")!)
        
        XCTAssertNotNil(sut)
        XCTAssertEqual(sut.date, "Jan 1, 2001")
        XCTAssertEqual(sut.time, "10:30 AM")
        XCTAssertEqual(sut.temperature, "26.2 ˚C")
        XCTAssertEqual(sut.temperature_minimum, "10.2 ˚C")
        XCTAssertEqual(sut.temperature_maximum, "29.4 ˚C")
        XCTAssertEqual(sut.pressure, "1002.18 hPa")
        XCTAssertEqual(sut.sea_level, "1003.19 hPa")
        XCTAssertEqual(sut.ground_level, "998.77 hPa")
        XCTAssertEqual(sut.humidity, "78 %")
    }
    
    func test_Model_Initialisation_Failure_All_Invalid() {
        var forecast = getMockForecast()
        forecast.date = nil
        sut = ForecastViewModel(with: forecast, in: TimeZone(abbreviation: "UTC")!)
        
        XCTAssertNil(sut)
    }
    
    func test_Model_Initialisation_Failure_With_Valid_Date() {
        var forecast = getMockForecast()
        
        var main = OWForecast.Main()
        main.temperature = nil
        main.temperature_minimum = nil
        main.temperature_maximum = nil
        main.pressure = nil
        main.sea_level = nil
        main.ground_level = nil
        main.humidity = nil
        
        forecast.set(main)
        
        sut = ForecastViewModel(with: forecast, in: TimeZone(abbreviation: "UTC")!)
        
        XCTAssertNotNil(sut)
        XCTAssertEqual(sut.date, "Jan 1, 2001")
        XCTAssertEqual(sut.time, "10:30 AM")
        XCTAssertEqual(sut.temperature, "- ˚C")
        XCTAssertEqual(sut.temperature_minimum, "- ˚C")
        XCTAssertEqual(sut.temperature_maximum, "- ˚C")
        XCTAssertEqual(sut.pressure, "- hPa")
        XCTAssertEqual(sut.sea_level, "- hPa")
        XCTAssertEqual(sut.ground_level, "- hPa")
        XCTAssertEqual(sut.humidity, "- %")
    }
}
