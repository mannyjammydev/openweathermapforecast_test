//
//  ForecastListViewModelTests.swift
//  OpenWeatherMapForecastTests
//
//  Created by Mantas Jakimavicius on 09/04/19.
//

import XCTest
@testable import OpenWeatherMapForecast

class ForecastListViewModelTests: XCTestCase {
    
    var sut: ForecastListViewModel!
    
    override func setUp() {
        sut = ForecastListViewModel(with: getMockForecastViewModels())
    }
    
    override func tearDown() {
        sut = nil
    }
    
    private func getMockForecastViewModels() -> [ForecastViewModel] {
        let forecasts = [
            OWForecast(),
            OWForecast(),
            OWForecast(),
            OWForecast(),
            OWForecast(),
            OWForecast()
        ]
        return forecasts.compactMap({ ForecastViewModel(with: $0) })
    }
    
    func test_Model_Initialisation() {
        XCTAssertEqual(sut.list.count, getMockForecastViewModels().count)
    }
}
