//
//  OWForecast.swift
//  OpenWeatherMapForecast
//
//  Created by Mantas Jakimavicius on 09/04/19.
//

import Foundation

struct OWForecast: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case date = "dt"
        case main
    }
    
    struct Main: Decodable {
        private enum CodingKeys: String, CodingKey {
            case temperature = "temp"
            case temperature_minimum = "temp_min"
            case temperature_maximum = "temp_max"
            case sea_level = "sea_level"
            case ground_level = "grnd_level"
            case humidity, pressure
        }
        
        var temperature: Float?
        var temperature_minimum: Float?
        var temperature_maximum: Float?
        var pressure: Float?
        var sea_level: Float?
        var ground_level: Float?
        var humidity: Float?
    }
    
    var date: TimeInterval?
    private var main: Main?
    
    var temperature: Float? { return main?.temperature }
    var temperature_minimum: Float? { return main?.temperature_minimum }
    var temperature_maximum: Float? { return main?.temperature_maximum }
    var pressure: Float? { return main?.pressure }
    var sea_level: Float? { return main?.sea_level }
    var ground_level: Float? { return main?.ground_level }
    var humidity: Float? { return main?.humidity }
    
    mutating func set(_ main: Main) { self.main = main }
}

struct OWForecastAPI_Response: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case statusCode = "cod"
        case message, list
    }
    
    var statusCode: String?
    var message: Float?
    var list: [OWForecast]?
}
