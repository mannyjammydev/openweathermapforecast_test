//
//  ForecastAPIClient.swift
//  OpenWeatherMapForecast
//
//  Created by Mantas Jakimavicius on 09/04/19.
//

import Foundation

final class ForecastAPIClient: APIClient {
    
    var session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func weather(with endpoint: ForecastEndpoint, completion: @escaping (Either<OWForecastAPI_Response, APIError>) -> Void) {
        
        guard let request = endpoint.request else { return completion(.error(.badURL)) }
        
        self.fetch(with: request) { (either: Either<OWForecastAPI_Response, APIError>) in
            switch either {
            case .value (let weather):
                completion(.value(weather))
            case .error (let error):
                completion(.error(error))
                break
            }
        }
    }
}
