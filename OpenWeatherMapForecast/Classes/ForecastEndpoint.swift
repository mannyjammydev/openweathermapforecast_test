//
//  APIClient.swift
//  WeatherApplication
//
//  Created by Mantas Jakimavicius on 10/04/19.
//

import Foundation
import CoreLocation

enum ForecastEndpoint: Endpoint {
    
    case forcast(_ location: CLLocationCoordinate2D)
    
    var baseUrl: String {
        return "https://api.openweathermap.org"
    }
    
    var path: String {
        switch self {
        case .forcast(_):
            return "/data/2.5/forecast"
        }
    }
    
    var queryItems: [URLQueryItem] {
        
        // All requests should be using the same key/appid
        var params = [URLQueryItem(name: "appid", value: "c92dcc692369a6eea28dcb9d0356c36a")]
        
        switch self {
        case .forcast(let location):
            params.append(URLQueryItem(name: "lat", value: String(location.latitude)))
            params.append(URLQueryItem(name: "lon", value: String(location.longitude)))
        }
        
        return params
    }
}
