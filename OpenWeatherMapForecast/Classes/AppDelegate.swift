//
//  AppDelegate.swift
//  OpenWeatherMapForecast
//
//  Created by Mantas Jakimavicius on 09/04/19.
//

import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    private let LONDON_COORDINATES = CLLocationCoordinate2D(latitude: 51.5044, longitude: -0.0863)
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if let viewcontroller = window?.rootViewController as? ForecastTableViewController {
            let viewModel = ForecastListViewModel(with: [])
            viewcontroller.inject(viewModel)
            
            let apiClient = ForecastAPIClient()
            let endpoint = ForecastEndpoint.forcast(LONDON_COORDINATES)
            apiClient.weather(with: endpoint) { (either) in
                switch either {
                case .value(let apiResponse):
                    
                    let forecasts = apiResponse.list ?? []
                    let viewModels = forecasts.compactMap({ ForecastViewModel(with: $0) })
                    let viewModel = ForecastListViewModel(with: viewModels)
                    viewcontroller.inject(viewModel)
                    
                case .error(let error):
                    print(error)
                }
            }
        }
        return true
    }
}
