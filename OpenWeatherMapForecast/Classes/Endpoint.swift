//
//  APIClient.swift
//  WeatherApplication
//
//  Created by Mantas Jakimavicius on 10/04/19.
//

import Foundation

protocol Endpoint {
    var baseUrl: String { get }
    var path: String { get }
    var queryItems: [URLQueryItem] { get }
}

extension Endpoint {
    var urlComponent: URLComponents? {
        var component = URLComponents(string: baseUrl)
        component?.path = path
        component?.queryItems = queryItems
        
        return component
    }
    
    var request: URLRequest? {
        guard let url = urlComponent?.url else { return nil }
        return URLRequest(url: url)
    }
}
