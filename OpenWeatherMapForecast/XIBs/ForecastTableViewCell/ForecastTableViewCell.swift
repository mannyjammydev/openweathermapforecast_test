//
//  ForecastTableViewCell.swift
//  OpenWeatherMapForecast
//
//  Created by Mantas Jakimavicius on 09/04/19.
//

import UIKit

final class ForecastTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateValueLabel: UILabel!
    @IBOutlet weak var timeValueLabel: UILabel!
    @IBOutlet weak var temperatureValueLabel: UILabel!
    @IBOutlet weak var temperatureMinimumValueLabel: UILabel!
    @IBOutlet weak var temperatureMaximumValueLabel: UILabel!
    @IBOutlet weak var pressureValueLabel: UILabel!
    @IBOutlet weak var seaLevelPressureValueLabel: UILabel!
    @IBOutlet weak var groundLevelPressureValueLabel: UILabel!
    @IBOutlet weak var humidityValueLabel: UILabel!
    
    var forecastViewModel: ForecastViewModel! {
        didSet {
            dateValueLabel.text = forecastViewModel.date
            timeValueLabel.text = forecastViewModel.time
            temperatureValueLabel.text = forecastViewModel.temperature
            temperatureMinimumValueLabel.text = forecastViewModel.temperature_minimum
            temperatureMaximumValueLabel.text = forecastViewModel.temperature_maximum
            pressureValueLabel.text = forecastViewModel.pressure
            seaLevelPressureValueLabel.text = forecastViewModel.sea_level
            groundLevelPressureValueLabel.text = forecastViewModel.ground_level
            humidityValueLabel.text = forecastViewModel.humidity
        }
    }
}
