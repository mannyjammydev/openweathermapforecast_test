//
//  ForecastViewModel.swift
//  OpenWeatherMapForecast
//
//  Created by Mantas Jakimavicius on 09/04/19.
//

import Foundation

final class ForecastViewModel {
    
    private let CELSIUS_UNIT = "˚C"
    private let PASCAL_UNIT = "hPa"
    private let PERCENTAGE_SIGN = "%"
    
    private(set) var date: String?
    private(set) var time: String?
    private(set) var temperature: String?
    private(set) var temperature_minimum: String?
    private(set) var temperature_maximum: String?
    private(set) var pressure: String?
    private(set) var sea_level: String?
    private(set) var ground_level: String?
    private(set) var humidity: String?
    
    init?(with model: OWForecast, in timeZone: TimeZone = TimeZone.current) {
        
        if let timeInterval = model.date {
            let forecastDate = Date(timeIntervalSince1970: timeInterval)
            date = date(from: forecastDate, in: timeZone)
            time = time(from: forecastDate, in: timeZone)
        } else { return nil }
        
        temperature = temperature(from: model.temperature)
        temperature_minimum = temperature(from: model.temperature_minimum)
        temperature_maximum = temperature(from: model.temperature_maximum)
        
        pressure = pressure(from: model.pressure)
        sea_level = pressure(from: model.sea_level)
        ground_level = pressure(from: model.ground_level)
        
        humidity = humidity(from: model.humidity)
    }
    
    private func date(from date: Date, in timeZone: TimeZone) -> String {
        let formatter = DateFormatter()
        formatter.timeZone = timeZone
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter.string(from: date)
    }
    
    private func time(from date: Date, in timeZone: TimeZone) -> String {
        let formatter = DateFormatter()
        formatter.timeZone = timeZone
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter.string(from: date)
    }
    
    private func temperature(from value: Float?) -> String {
        return string(from: celsius(from: value), unit: CELSIUS_UNIT)
    }
    
    private func pressure(from value: Float?) -> String {
        return string(from: value, unit: PASCAL_UNIT)
    }
    
    private func humidity(from value: Float?) -> String {
        return string(from: value, unit: PERCENTAGE_SIGN)
    }
    
    private func string(from value: Float?, unit: String) -> String {
        guard let value = value else { return "- \(unit)" }
        return "\(value.string(minimumFractionDigits: 0, maximumFractionDigits: 2)) \(unit)"
    }
    
    private func celsius(from kelvin: Float?) -> Float? {
        guard let kel = kelvin else { return kelvin }
        return kel - 273.15
    }
}
