//
//  ForecastListViewModel.swift
//  OpenWeatherMapForecast
//
//  Created by Mantas Jakimavicius on 09/04/19.
//

import Foundation

final class ForecastListViewModel {
    
    let list: [ForecastViewModel]
    
    subscript(item: Int) -> ForecastViewModel {
        return list[item]
    }
    
    init(with list: [ForecastViewModel]) {
        self.list = list
    }
}
