//
//  ForecastTableViewController.swift
//  OpenWeatherMapForecast
//
//  Created by Mantas Jakimavicius on 09/04/19.
//

import UIKit

final class ForecastTableViewController: UITableViewController {

    private let FORECAST_TABLEVIEW_CELL_NAME = "ForecastTableViewCell"
    
    var viewModel: ForecastListViewModel! { didSet { tableView.reloadData() } }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: FORECAST_TABLEVIEW_CELL_NAME, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: FORECAST_TABLEVIEW_CELL_NAME)
    }
    
    func inject(_ viewModel: ForecastListViewModel) { self.viewModel = viewModel }
    
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.list.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FORECAST_TABLEVIEW_CELL_NAME, for: indexPath) as? ForecastTableViewCell else {
            return UITableViewCell()
        }
        cell.forecastViewModel = viewModel[indexPath.item]
        return cell
    }
}
